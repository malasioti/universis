import { ThesesModule } from './theses.module';
import {inject} from '@angular/core/testing';
import {TranslateService} from '@ngx-translate/core';

describe('ThesesModule', () => {

  beforeEach(() => {
    //
  });

  it('should create an instance', inject([TranslateService], (translateService: TranslateService) => {
    const thesesModule = new ThesesModule(translateService);
    expect(thesesModule).toBeTruthy();
  }));
});
