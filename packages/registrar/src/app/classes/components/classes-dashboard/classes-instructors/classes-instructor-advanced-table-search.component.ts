import { Component, ViewEncapsulation } from '@angular/core';
import { AngularDataContext } from '@themost/angular';
import { ActivatedRoute } from '@angular/router';
import { DatePipe } from '@angular/common';
import {AdvancedTableSearchBaseComponent} from '@universis/ngx-tables';

@Component({
  selector: 'app-classes-instructor-advanced-table-search',
  templateUrl: './classes-instructor-advanced-table-search.component.html',
  encapsulation: ViewEncapsulation.None
})
export class ClassesInstructorAdvancedTableSearchComponent extends AdvancedTableSearchBaseComponent {

  constructor(_context: AngularDataContext, _activatedRoute: ActivatedRoute, datePipe: DatePipe) {
    super();
  }

}
