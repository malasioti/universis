import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AngularDataContext } from '@themost/angular';

@Component({
  selector: 'app-registrations-preview-general-info-form',
  templateUrl: './registrations-preview-general-info-form.component.html'
})
export class RegistrationsPreviewGeneralInfoFormComponent implements OnInit {

  @Input() model: any;

  constructor(private _activatedRoute: ActivatedRoute,
              private _context: AngularDataContext) {
  }

  async ngOnInit() {

  }


}
