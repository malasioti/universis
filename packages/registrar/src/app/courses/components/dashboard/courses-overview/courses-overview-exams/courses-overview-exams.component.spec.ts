import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CoursesOverviewExamsComponent } from './courses-overview-exams.component';

describe('CoursesOverviewExamsComponent', () => {
  let component: CoursesOverviewExamsComponent;
  let fixture: ComponentFixture<CoursesOverviewExamsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CoursesOverviewExamsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CoursesOverviewExamsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
