import {Component, Input, OnInit} from '@angular/core';
// tslint:disable-next-line:max-line-length
import {ActivatedRoute, Router} from '@angular/router';
import {AngularDataContext} from '@themost/angular';
import { DatePipe } from '@angular/common';
import {
    AdvancedTableModalBaseComponent,
    AdvancedTableModalBaseTemplate
} from '@universis/ngx-tables';
import {AdvancedFilterValueProvider} from '@universis/ngx-tables';
import * as DEFAULT_SELECTABLE_COURSES_LIST from '../courses-table/courses-table.config.selectableCourse.json';
import {AdvancedTableConfiguration} from '@universis/ngx-tables';

@Component({
    selector: 'app-select-course',
    template: AdvancedTableModalBaseTemplate
})
export class SelectCourseComponent extends AdvancedTableModalBaseComponent implements OnInit {

    @Input() tableConfig: any;
    constructor(_router: Router, _activatedRoute: ActivatedRoute,
                _context: AngularDataContext,
                protected advancedFilterValueProvider: AdvancedFilterValueProvider,
                protected datePipe: DatePipe) {
        super(_router, _activatedRoute, _context, advancedFilterValueProvider, datePipe);
    }

    ngOnInit(tableConfig = DEFAULT_SELECTABLE_COURSES_LIST) {
      if (!this.tableConfig) {
        this.tableConfig = AdvancedTableConfiguration.cast(tableConfig || DEFAULT_SELECTABLE_COURSES_LIST);
      }
      super.ngOnInit();
    }

    hasInputs(): Array<string> {
        return [];
    }

    ok(): Promise<any> {
        return this.close();
    }
}
