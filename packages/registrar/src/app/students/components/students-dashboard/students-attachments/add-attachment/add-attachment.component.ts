import { Component, Input, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { RouterModalOkCancel } from '@universis/common/routing';
import { ActivatedRoute, Router } from '@angular/router';
import { AppEventService } from '@universis/common';
import { LoadingService, ModalService } from '@universis/common';
import { Observable, Subscription } from 'rxjs';
import { AdvancedFormComponent } from '@universis/forms';
import { AngularDataContext } from '@themost/angular';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-add-attachment',
  templateUrl: './add-attachment.component.html',
})
export class AddAttachmentComponent
  extends RouterModalOkCancel
  implements OnInit, OnDestroy
{
  public lastError: any;
  public loading: boolean;
  @Input() data: any;
  @ViewChild('formComponent') formComponent: AdvancedFormComponent;
  formConfig: any;
  private formLoadSubscription: Subscription;
  private formChangeSubscription: Subscription;
  @Input() formEditName = 'Students/add-attachment-helper';
  public attachments = [];

  constructor(
    router: Router,
    activatedRoute: ActivatedRoute,
    private _modalService: ModalService,
    private _appEvent: AppEventService,
    private _loadingService: LoadingService,
    private _context: AngularDataContext,
    private _http: HttpClient
  ) {
    super(router, activatedRoute);
    // set modal size
    this.modalClass = 'modal-lg';
    this.okButtonDisabled = true;
    this.modalTitle = 'Register.AddAttachment';
  }

  ngOnInit() {
    this.formLoadSubscription = this.formComponent.form.formLoad.subscribe(
      () => {
        if (this.formComponent.form.config) {
          // find submit button
          const findButton = this.formComponent.form.form.components.find(
            (component) => {
              return component.type === 'button' && component.key === 'submit';
            }
          );
          // hide button
          if (findButton) {
            (<any>findButton).hidden = true;
          }
        }
      }
    );
    this.formComponent.formName = this.formEditName;
    this.formChangeSubscription = this.formComponent.form.change.subscribe(
      (event) => {
        if (Object.prototype.hasOwnProperty.call(event, 'isValid')) {
          // enable or disable button based on form status
          this.okButtonDisabled = !(
            event.isValid && this.attachments.length > 0
          );
          if (this.formComponent.form.formio.data.attachmentTypes.id == null) {
            this.okButtonDisabled = true;
          }
        }
      }
    );
  }

  cancel(): Promise<any> {
    // close
    if (this._modalService.modalRef) {
      return this._modalService.modalRef.hide();
    }
  }

  ngOnDestroy(): void {
    if (this.formLoadSubscription) {
      this.formLoadSubscription.unsubscribe();
    }
    if (this.formChangeSubscription) {
      this.formChangeSubscription.unsubscribe();
    }
  }

  onFileSelect(event) {
    if (this.attachments.length !== 0) {
      this.attachments.splice(0, 1);
    }
    const addedFile = event.addedFiles[0];
    this.attachments.push(addedFile);
    if (this.formComponent && this.formComponent.form) {
      this.formComponent.form.change.emit({ isValid: true });
    }
  }

  onFileRemove(event) {
    this.attachments.splice(this.attachments.indexOf(event), 1);
    if (this.formComponent && this.formComponent.form) {
      this.formComponent.form.change.emit({ isValid: false });
    }
  }

  async ok() {
    this._loadingService.showLoading();
    const studentId = this.data.studentId;
    const attachment = this.attachments[0];
    const attachmentType =
      this.formComponent.form.formio.data.attachmentTypes.id;
    // clear error
    this.lastError = null;
    // create formData to be posted
    const formData: FormData = new FormData();
    formData.append('file', attachment, attachment.name);
    formData.append('file[attachmentType]', attachmentType);
    // get headers
    const serviceHeaders = this._context.getService().getHeaders();
    // set postUrl
    const postUrl = this._context
      .getService()
      .resolve(`Students/${studentId}/attachments/add`);
    // add attachment
    try {
      await this._http
        .post(postUrl, formData, {
          headers: serviceHeaders,
        })
        .toPromise();
      this._loadingService.hideLoading();
      // fire event
      this._appEvent.change.next({ model: 'AddStudentAttachment' });
      // close
      if (this._modalService.modalRef) {
        return this._modalService.modalRef.hide();
      }
    } catch (err) {
      this.lastError = err;
      this._loadingService.hideLoading();
    }
  }
}
