import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AngularDataContext } from '@themost/angular';
import { LoadingService } from '@universis/common';
import { RouterModalOkCancel } from '@universis/common/routing';
import { AdvancedFormComponent } from '@universis/forms';
import { Subscription } from 'rxjs';
// tslint:disable-next-line: max-line-length
import {
  GraduationGradeWrittenInWordsCalculationAttributes,
  StudentsService,
} from '../../../../services/students-service/students.service';

@Component({
  selector: 'app-edit-student-declaration',
  templateUrl: './edit-student-declaration.component.html',
  styleUrls: ['./edit-student-declaration.component.scss'],
})
export class EditStudentDeclarationComponent
  extends RouterModalOkCancel
  implements OnInit, OnDestroy
{
  public studentDeclaration: any;
  public formName: string;
  public model: string;
  public formData: any;
  public lastError: any;
  private dataSubscription: Subscription;
  private formChangeSubscription: Subscription;
  private graduationGradeWrittenInWords: string;
  private graduationGradeWrittenInWordsComponent: any;

  @ViewChild('formComponent') formComponent: AdvancedFormComponent;
  constructor(
    private _activatedRoute: ActivatedRoute,
    private _router: Router,
    private _context: AngularDataContext,
    private _loadingService: LoadingService,
    private _studentsService: StudentsService
  ) {
    // call super constructor
    super(_router, _activatedRoute);
    // override-set class
    this.modalClass = 'modal-xl';
  }

  ngOnInit() {
    this.dataSubscription = this._activatedRoute.data.subscribe(
      async (resolvedData) => {
        // get resolved student declaration data
        this.studentDeclaration = resolvedData && resolvedData.data;
        // get resolved form name
        this.formName = resolvedData && resolvedData.action;
        // and model
        this.model = resolvedData && resolvedData.model;
        // calculate graduation grade written in words (do not set form value)
        this.graduationGradeWrittenInWords =
          await this.calculateGraduationGradeWrittenInWords(false);
        // and assign to form for validations
        Object.assign(this.studentDeclaration, {
          validateGradeWrittenInWords: this.graduationGradeWrittenInWords,
        });
        // and initate form
        this.formData = this.studentDeclaration;
      }
    );

    this.formChangeSubscription = this.formComponent.form.change.subscribe(
      (event) => {
        if (Object.prototype.hasOwnProperty.call(event, 'isValid')) {
          // enable or disable button based on form status
          this.okButtonDisabled = !event.isValid;
        }
      }
    );
  }

  ngOnDestroy(): void {
    if (this.dataSubscription) {
      this.dataSubscription.unsubscribe();
    }
    if (this.formChangeSubscription) {
      this.formChangeSubscription.unsubscribe();
    }
  }

  async ok() {
    try {
      // clear error, if any
      this.lastError = null;
      // get form data
      const studentDeclarationFormData =
        this.formComponent &&
        this.formComponent.form &&
        this.formComponent.form.formio &&
        this.formComponent.form.formio.data;
      if (studentDeclarationFormData == null) {
        return this.close();
      }
      // save
      await this._context.model(this.model).save(studentDeclarationFormData);
      // and close with reload fragment
      return this.close({ fragment: 'reload', skipLocationChange: true });
    } catch (err) {
      // log and set lastError
      console.error(err);
      this.lastError = err;
    }
  }

  onCustomEvent(event: any): void {
    if (event && event.type === 'calculateGradeWrittenInWords') {
      // calculate or set graduation grade written in words (set value)
      this.calculateGraduationGradeWrittenInWords(true)
        .then(() => {
          //
        })
        .catch((err) => {
          console.error(err);
          this.lastError = err;
        });
    }
  }

  async calculateGraduationGradeWrittenInWords(
    setFormComponentValue: boolean
  ): Promise<string> {
    try {
      // if grade is already calculated
      if (this.graduationGradeWrittenInWords) {
        if (setFormComponentValue) {
          if (this.graduationGradeWrittenInWordsComponent == null) {
            this.graduationGradeWrittenInWordsComponent =
              this.getGradeWrittenInWordsComponent();
          }
          // set form value
          this.graduationGradeWrittenInWordsComponent.setValue(
            this.graduationGradeWrittenInWords
          );
        }
        // and exit
        return;
      }
      this._loadingService.showLoading();
      // adapt mappings for graduation edit for this procedure
      if (this.model === 'Students') {
        this.studentDeclaration.student = this.studentDeclaration;
      }
      if (
        !(
          this.studentDeclaration.student &&
          this.studentDeclaration.student.studyProgram
        )
      ) {
        return;
      }
      // prepare calculation attributes
      // get student status
      const studentStatus =
        this.studentDeclaration.student.studentStatus &&
        this.studentDeclaration.student.studentStatus.alternateName;
      // get graduation grade
      const graduationGrade = this.studentDeclaration.graduationGrade;
      // get program decimal digits
      const decimalDigits =
        this.studentDeclaration.student.studyProgram.decimalDigits;
      // get grade scale id and type either from the graduationGradeScale
      // or the studyProgram gradeScale
      const graduationGradeScaleId =
        (this.studentDeclaration.graduationGradeScale &&
          this.studentDeclaration.graduationGradeScale.id) ||
        (this.studentDeclaration.student.studyProgram.gradeScale &&
          this.studentDeclaration.student.studyProgram.gradeScale.id);
      const graduationGradeScaleType =
        (this.studentDeclaration.graduationGradeScale &&
          this.studentDeclaration.graduationGradeScale.scaleType) ||
        (this.studentDeclaration.student.studyProgram.gradeScale &&
          this.studentDeclaration.student.studyProgram.gradeScale.scaleType);
      // and perform the calculation
      const calculationAttributes: GraduationGradeWrittenInWordsCalculationAttributes = {
          studentStatus: studentStatus,
          graduationGrade: graduationGrade,
          decimalDigits: decimalDigits,
          graduationGradeScale: {
            id: graduationGradeScaleId,
            scaleType: graduationGradeScaleType,
          },
        };
      const graduationGradeWrittenInWords =
        await this._studentsService.calculateGraduationGradeWrittenInWords(
          calculationAttributes
        );
      // set grade written in words
      this.graduationGradeWrittenInWords = graduationGradeWrittenInWords;
      if (setFormComponentValue) {
        if (this.graduationGradeWrittenInWordsComponent == null) {
          this.graduationGradeWrittenInWordsComponent =
            this.getGradeWrittenInWordsComponent();
        }
        // set form value
        this.graduationGradeWrittenInWordsComponent.setValue(
          this.graduationGradeWrittenInWords
        );
      }
      // reset data structure
      if (this.model === 'Students') {
        delete this.studentDeclaration.student;
      }
      // and return
      return graduationGradeWrittenInWords;
    } catch (err) {
      // log and set lastError
      console.error(err);
      this.lastError = err;
    } finally {
      this._loadingService.hideLoading();
    }
  }

  getGradeWrittenInWordsComponent() {
    const utils =
      this.formComponent &&
      this.formComponent.form &&
      this.formComponent.form.formio;
    if (utils && typeof utils.getComponent === 'function') {
      return utils.getComponent('graduationGradeWrittenInWords');
    }
  }

  async cancel() {
    // close
    return this.close();
  }
}
