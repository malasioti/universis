import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StudyProgramsPreviewGeneralStatisticsStudentStatusComponent } from './study-programs-preview-general-statistics-student-status.component';

describe('StudyProgramsPreviewGeneralStatisticsStudentStatusComponent', () => {
  let component: StudyProgramsPreviewGeneralStatisticsStudentStatusComponent;
  let fixture: ComponentFixture<StudyProgramsPreviewGeneralStatisticsStudentStatusComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StudyProgramsPreviewGeneralStatisticsStudentStatusComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StudyProgramsPreviewGeneralStatisticsStudentStatusComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
