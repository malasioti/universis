import {Component, Input, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {RouterModalOkCancel} from '@universis/common/routing';
import {ActivatedRoute, Router} from '@angular/router';
import {AppEventService} from '@universis/common';
import {ErrorService, LoadingService, ModalService, ToastService} from '@universis/common';
import {Observable, Subscription} from 'rxjs';
import {AdvancedFormComponent} from '@universis/forms';
import {TranslateService} from '@ngx-translate/core';
import {AngularDataContext} from '@themost/angular';

@Component({
  selector: 'app-create-snapshot',
  templateUrl: './create-snapshot.component.html'
})
export class CreateSnapshotComponent extends RouterModalOkCancel implements OnInit, OnDestroy {

  public loading = false;
  public lastError;
  @Input() execute: Observable<any>;
  @Input() data: any;
  @ViewChild('formComponent') formComponent: AdvancedFormComponent;
  formConfig: any;
  private formLoadSubscription: Subscription;
  private formChangeSubscription: Subscription;
  @Input() formEditName = 'DepartmentSnapshots/new';
  @Input() toastHeader = 'Snapshots.CreateSnapshot';



  constructor(router: Router,
              activatedRoute: ActivatedRoute,
              private _errorService: ErrorService,
              private _modalService: ModalService,
              private _appEvent: AppEventService,
              private _toastService: ToastService,
              private _loadingService: LoadingService,
              private _translateService: TranslateService,
              private _context: AngularDataContext) {
    super(router, activatedRoute);
    // set modal size
    this.modalClass = 'modal-lg';
    this.okButtonDisabled = true;
  }

  ngOnInit() {
    this.formLoadSubscription = this.formComponent.form.formLoad.subscribe(() => {
      if (this.formComponent.form.config) {
        // find submit button
        const findButton = this.formComponent.form.form.components.find(component => {
          return component.type === 'button' && component.key === 'submit';
        });
        // hide button
        if (findButton) {
          (<any>findButton).hidden = true;
        }
      }
    });
    this.formComponent.formName = this.formEditName;
    this.formChangeSubscription = this.formComponent.form.change.subscribe((event) => {
      if (Object.prototype.hasOwnProperty.call(event, 'isValid')) {
        // enable or disable button based on form status
        this.okButtonDisabled = !event.isValid;
      }
    });
    // format form data
    this.formComponent.data = this.data;
  }

  cancel(): Promise<any> {
    if (this.loading) {
      return;
    }
    // close
    if (this._modalService.modalRef) {
      return this._modalService.modalRef.hide();
    }
  }

  ngOnDestroy(): void {
    if (this.formLoadSubscription) {
      this.formLoadSubscription.unsubscribe();
    }
    if (this.formChangeSubscription) {
      this.formChangeSubscription.unsubscribe();
    }
  }

  async ok() {
    try {
      this.loading = true;
      this.lastError = null;
      const data = this.formComponent.form.formio.data;
      // create snapshot

      if (this.data) {
        await this._context.model(`Departments/${data.department}/snapshot`).save(data);
        // and hide
        this.loading = false;
        // close
        if (this._modalService.modalRef) {
          this._modalService.modalRef.hide();
        }
        this._toastService.show(
          this._translateService.instant('Snapshots.CreateSnapshot'),
          this._translateService.instant('Settings.OperationCompleted')
        );
        this._appEvent.change.next({model: 'DepartmentSnapshots'});
      } else {
        this.loading = false;
        // close
        if (this._modalService.modalRef) {
          this._modalService.modalRef.hide();
        }
      }
    } catch (err) {
      this.loading = false;
      this.lastError = err;
    }
  }

  onChange($event: any) {

  }

}
