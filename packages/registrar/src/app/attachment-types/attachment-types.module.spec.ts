import { AttachmentTypesModule } from './attachment-types.module';

describe('AttachmentTypesModule', () => {
  let attachmentTypesModule: AttachmentTypesModule;

  beforeEach(() => {
    attachmentTypesModule = new AttachmentTypesModule();
  });

  it('should create an instance', () => {
    expect(attachmentTypesModule).toBeTruthy();
  });
});
